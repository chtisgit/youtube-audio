package main

import (
	"encoding/binary"
	"errors"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/dgraph-io/badger"
)

var options struct {
	host  string
	port  string
	limit int
}

var barrier chan int
var cache *badger.DB

type CachedData struct {
	Expires time.Time
	Json    []byte
}

func (c CachedData) MarshalBinary() ([]byte, error) {
	ts := make([]byte, 8)
	binary.LittleEndian.PutUint64(ts, uint64(c.Expires.Unix()))
	return append(ts, c.Json...), nil
}

func (c *CachedData) UnmarshalBinary(buf []byte) error {
	if len(buf) < 8 {
		return errors.New("corrupt data")
	}
	c.Expires = time.Unix(int64(binary.LittleEndian.Uint64(buf)), 0)
	c.Json = buf[8:]
	return nil
}

func loadData(v string) ([]byte, error) {
	var data []byte

	err := cache.View(func(tx *badger.Txn) error {
		item, err := tx.Get([]byte(v))
		if err != nil {
			return err
		}
		val, err := item.Value()
		if err != nil {
			return err
		}

		cd := CachedData{}
		if err := cd.UnmarshalBinary([]byte(val)); err != nil {
			return err
		}
		if time.Now().After(cd.Expires) {
			return errors.New("outdated data")
		}
		data = cd.Json

		return nil
	})
	return data, err
}

func saveData(v string, data []byte) error {
	return cache.Update(func(tx *badger.Txn) error {
		d, _ := CachedData{
			Expires: time.Now().Add(time.Minute * 30),
			Json:    data,
		}.MarshalBinary()
		tx.Set([]byte(v), d)
		return nil
	})
}

func retrieveData(v string) ([]byte, error) {
	var data []byte

	cmd := exec.Command("youtube-dl", "-j", "https://www.youtube.com/watch?v="+v)
	r, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	defer r.Close()

	err = cmd.Start()
	if err != nil {
		return nil, err
	}
	data, err = ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func youtubedl(v string) (url []byte, err error) {
	<-barrier
	log.Println("Running youtube-dl")
	url, err = retrieveData(v)
	barrier <- 0
	return
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	vid := r.URL.Query().Get("v")

	if r.Method != http.MethodGet || len(vid) < 6 {
		http.NotFound(w, r)
		return
	}

	log.Println("Requested '" + vid + "'")

	var url []byte
	var err error

	url, err = loadData(vid)
	if err == nil {
		log.Println("Retrieved from cache")
	} else {
		log.Println("loadData ", err)
		url, err = youtubedl(vid)
	}
	if err != nil {
		http.NotFound(w, r)
	} else {
		w.Write(url)
		saveData(vid, url)
	}

	log.Println("OK")
}

func main() {
	flag.StringVar(&options.host, "host", "", "interface on which HTTP API should listen")
	flag.StringVar(&options.port, "port", "8080", "port on which HTTP API should listen")
	flag.IntVar(&options.limit, "limit", 5, "limit number of parallelly processed connections")

	if options.limit > 500 {
		options.limit = 500
		log.Println("Limit was capped to 500.")
	}

	barrier = make(chan int, options.limit)

	for i := 0; i < options.limit; i++ {
		barrier <- 0
	}

	badgerOpts := badger.DefaultOptions
	badgerOpts.Dir = "/tmp/youtube-cache"
	badgerOpts.ValueDir = "/tmp/youtube-cache"

	var err error
	cache, err = badger.Open(badgerOpts)
	if err != nil {
		log.Println("Not using cache. ", err)
		cache = nil
	} else {
		log.Println("Using badger cache")
		defer cache.Close()
		go func() {
			for {
				<-time.After(time.Minute * 5)
				err := cache.PurgeOlderVersions()
				if err != nil {
					log.Println("Purge: ", err)
				}
				err = cache.RunValueLogGC(0.5)
				if err != nil {
					log.Println("Purge: ", err)
				}
			}
		}()
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		file, err := os.Open("index.html")
		if err != nil {
			http.NotFound(w, r)
			return
		}
		defer file.Close()
		io.Copy(w, file)
	})
	http.HandleFunc("/api/", handleRequest)
	http.ListenAndServe(net.JoinHostPort(options.host, options.port), nil)
}
